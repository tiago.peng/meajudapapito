*** Settings ***
Documentation  O usuário informa suas credenciais de acesso e submete o formulário de login
Resource  ../resources/steps/cadastro.robot

Test Setup      Abrir navegador
Test Teardown   Fechar navegador

*** Test Cases ***
Login com sucesso
  Dado que acesso o aplicativo com o CPF - "03658282541"
  Quando preencho meus dados de cadastro
  E defino uma senha para o meu cadastro
  Então vejo o tutorial de ativação de crédito