

*** Variables ***

# Cadastro Page
${INPUT_CPF}                    id:cpfInput
${BUTTON_SUBMIT}                id:submitButton
${INPUT_BIRTHDAY}               id:birthdayInput
${INPUT_ENROLLMENTNUMBER}       id:enrollmentNumberInput
${INPUT_CARDLASTFOURDIGITS}     id:cardLastFourDigitsInput
${INPUT_PASS}                   css:input[autocomplete=off]