*** Settings ***
Library     String
Library     SeleniumLibrary
Resource    ../elements.robot

*** Variables ***
${DEFAULT_URL}          https://credcesta-dev2.cubos.dev/
${BROWSER}              headlesschrome

*** Keywords ***
#### Setup e Teardown
Abrir navegador
    Open Browser    ${DEFAULT_URL}     ${BROWSER}
    Set Selenium Implicit Wait  25
    Set Window Size     1200    800

Fechar navegador
    Capture Page Screenshot
    Close Browser