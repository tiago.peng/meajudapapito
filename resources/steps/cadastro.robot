*** Settings ***
Resource    ./base.robot

*** Keywords ***
#### Steps
Dado que acesso o aplicativo com o CPF - "${cpf}"
    Input Text                          ${INPUT_CPF}    ${cpf}
    Wait Until Element Is Enabled       ${BUTTON_SUBMIT}
    Click Button                        ${BUTTON_SUBMIT}
    Wait Until Element Is Visible       xpath://div/span[contains(text(),"1")]
    
Quando preencho meus dados de cadastro
    Input Text                          ${INPUT_BIRTHDAY}               01/01/2000
    Input Text                          ${INPUT_ENROLLMENTNUMBER}       1234
    Input Text                          ${INPUT_CARDLASTFOURDIGITS}     1234
    Click Button                        ${BUTTON_SUBMIT}
    Wait Until Element Is Visible       xpath://div/span[contains(text(),"2")]
    Select Radio Button                 radio       email       
    Click Button                        ${BUTTON_SUBMIT}
    Wait Until Element Is Visible       xpath://div/span[contains(text(),"3")]
    Input Text                          css:input[autocomplete=off]       111111
    Click Button                        ${BUTTON_SUBMIT}

E defino uma senha para o meu cadastro
    Wait Until Element Is Visible       xpath://div/span[contains(text(),"4")]
    Input Text                          css:input[autocomplete=off]        162534
    Click Button                        ${BUTTON_SUBMIT}
    Input Text                          css:input[autocomplete=off]        162534
    Click Button                        ${BUTTON_SUBMIT}
    
Então vejo o tutorial de ativação de crédito
    Wait Until Element Is Visible       xpath://span[contains(text(), "Crédito")]